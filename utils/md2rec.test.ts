import { md2rec } from "./md2rec";

describe(md2rec.name, () => {
  it("not header", () => expect(md2rec(
    "#a"
  )).toStrictEqual([
    {"tags": undefined, "content": ["#a"]}
  ]))

  it("content-less", () => expect(md2rec([
    "# a"
  ])).toStrictEqual([
    {"tags": ["a"], "content": undefined}
  ]))

  it("simple", () => expect(md2rec([
    "# a",
    "WWW"
  ])).toStrictEqual([
    {"tags": ["a"], "content": ["WWW"]}
  ]))

  it("nested", () => expect(md2rec([
    "# a",
    "WWW",
    "## b",
    "XXX"
  ])).toStrictEqual([
    {"tags": ["a"], "content": ["WWW"]},
    {"tags": ["a", "b"], "content": ["XXX"]}
  ]))

  it("complex", () => expect(md2rec(`
[[_TOC_]]
# a
# b
WWW
## c
Xxx
## d
YYY
# e
ZZZ
OOO
  `)).toStrictEqual([
    {"tags": undefined, "content": ["[[_TOC_]]"]},
    {"tags": ["a"], "content": undefined},
    {"tags": ["b"], "content": ["WWW"]},
    {"tags": ["b", "c"], "content": ["Xxx"]},
    {"tags": ["b", "d"], "content": ["YYY"]},
    {"tags": ["e"], "content": ["ZZZ", "OOO"]},
  ]))
})